Repo for my blog, such as it is.

[![Netlify Status](https://api.netlify.com/api/v1/badges/103b6032-3968-4c63-af11-68495d3113ca/deploy-status)](https://app.netlify.com/sites/suspicious-lovelace-1ce78b/deploys)
Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
