---
title: "When you find out, let me knows"
date: 2021-02-21T12:23:24-05:00
draft: false
tags: ['meta']
---

Greetings and salutations. My name is Sean, and this is my blog and homepage on the wondrous place that we call the Internet. If you found me somewhere else, here's where everything comes from! 

#### whoami and what is this about?

I'm a former sysadmin who decided it would be a good idea to hang up his sudo privs and go into management. So far I haven't regretted it. I manage a small team focused on improving monitoring, security and resiliency for some of my [company's](https://www.redhat.com) internal deployments. I have a [Bachelor's](https://www.lycoming.edu) in Music and Computer Science, a [Master's](https://www.champlain.edu) in Information Security Operations, as well as being an Extra class Amateur Radio Operator. I	 use he/him/his pronouns. 

This is going to be my central repository for blogging. I'll warn you up front, posting is going be inconsistent and the topics could be all over the place. I'll mostly be covering topics on information security, monitoring and observability, IT management, Linux systems administration and probably some other techy things. Some posts may be about amateur radio. I'll very likely be posting about my experiences with ADHD as well. 

My goal is to use this space to write about neat things I'm doing, and also to post my experiences as I learn new things. I also plan to have some commentary on various goings on in the world of tech and neurodiversity.

At the moment, this blog is written in markdown using Ghostwriter and then converted to static html pages using hugo. It is centrally hosted in a [codeberg](https://www.codeberg.org) git repo, all running on [netlify](https://www.netlify.com/). I hope to eventually begin federating and syndicating it across other services, but I need to figure out an easy way to script that to make it as ADHD-friendly a task as possible. 

This blog is very much a work in progress. Expect changes constantly (and edited posts...). I'll probably even change the name. Minimum viable product, right?



