---
title: "Assumingpositiveintent"
date: 2021-03-31T09:25:53-04:00
draft: true
---

Many have pointed out the [flaws](https://thebias.com/2017/09/26/how-good-intent-undermines-diversity-and-inclusion/) in how we use the phrase "Assume Positive Intent." It's often used when one person (person A) injures another (person B), where person B is told by person C that person B didn't mean it. Often, person C is themselves operating with good intentions when they say this. 

I don't deny the dangers of the phrase. It is, however, a powerful tool in addressing conflict and approaching difficult conversations. Unless explicitly shown otherwise, we must assume that the person we are speaking with had positive intent, that they didn't mean injury. To do otherwise sets us up right out of the gate to approach the person with a negative mindset and that will color our interactions with them.

That said, WE must assume positive intent. We should not tell someone who has been injured to do so. If person A hurts person B, person C should not tell person B to assume good intentions, person C should use this assumption while directly addressing the harm caused with person A. Yes, it would be helpful for person B to assume that person A did not mean harm, but the harm has still occurred and ignorance is not innocence!

This is getting confusing with the persons. I'll try and clarify.

A member of your community, we'll call them Fleetlebop, makes an ableist comment. Fleetlebop has been known to use this phrase before. Billicon, a disabled member of the community, overhears Fleetlebop's comment. Billicon approaches Jotwizzle, a leader in the community, and raises concerns about the comment.

Let's play this out in a few ways. 



`Billicon: Hey Jotwizzle, Fleetlebop has said a few things that are really ableist and they make me wonder if I'm not a valued member of this community.`

`Jotwizzle: Oh, I know, he says that a lot but he doesn't actually mean it. He's usually a pretty good person otherwise, he probably doesn't even know that what he's saying is harmful.`


As you can see, Jotwizzle is assuming positive intentions here while also (in not so many words) telling Billicon to do the same. It doesn't actually address the concerns. This is somewhat of a forced example, but it happens regularly in the real world!

We have another approach, one without any positive intent being assumed, has different flaws.

`Billicon: Hey Jotwizzle, Fleetlebop has said a few things that are really ableist and they make me wonder if I'm not a valued member of this community.`

`Jotwizzle: I'll handle it, thanks for letting me know`

`*scene change*`

`Jotwizzle: Hey Fleetlebop, you're being a real jerk to Billicon with your comments`

Woah! Again, a bit of a forced example, but one that does happen in the real world. Jotwizzle did not assume positive intent while approaching Fleetlebop and now Fleetlebop is on the defensive. Maybe they did actually not intend any harm and may have not even realized that they were doing harm. Now they feel uncomfortable in the community.

Let's look at how this should go.

`Billicon: Hey Jotwizzle, Fleetlebop has said a few things that are really ableist and they make me wonder if I'm not a valued member of this community.`

`Jotwizzle: I'll handle it, thanks for letting me know`

`*scene change*`

`Jotwizzle: Hey Fleetlebop, can we chat for a minute about some concerns? You made a comment recently that was ableist, and I just wanted to bring it to your attention as comments like that can be hurtful.`

Now, Fleetlebop isn't immediately put on the defensive and a more open discussion can occur around this. Obviously, the next steps depend on how Fleetlebop responds. The correct response should be "oh, shoot, I didn't realize, I'll be better."

There is another way to approach this that is subtly different, but still harmful.

