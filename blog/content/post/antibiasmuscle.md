---
title: "Making your anti-bias swole"
date: 2021-03-10T01:04:39-05:00
draft: false
tags: [diversity, bias, self-improvement]
---


Eliminating your conscious biases is hard. Your unconscious biases? Even harder. It's a ton of work, I get it. Racism, sexism, ableism, these are hard baked into our vocabulary and actions. Even if you're actively trying to be better, there is still a ton of work to do. It takes years of really paying attention to words and actions, active work to double check a word to see if it's actually rooted in injury before you use it, and so on. 

This isn't saying "shucks, those of us not in marginalized demographics have it rough!" We already start with a leg up in most things, if not more than a leg. Rather, this is saying that we are pretty deep into the bias hole and it takes real effort to dig out. Just sitting idly by won't do it.

I've heard people wonder why, why should they try and fix their language? Surely that one word can't be too bad. It's a lot of work to fix it! I'm not going to bother.

The thing is, language matters. Every word matters, Maybe that one word is just a grain of sand, but in a sea of grains of sand it can wear down the hardest steel. We need to be better.

We don't get better overnight. This isn't something you sit down and say "well, tomorrow I'll be free of bias." It takes sustained, conscious effort over a period of time.

Sustained, conscious, long term effort to improve yourself? That sounds pretty familiar. We exercise, right? When you start lifting weights, it's hard. You struggle through. But over time, with incremental improvements, you can lift heavier and heavier weights. 

Not much of an exerciser? What about correcting your diet. Eating healthier, fewer candy bars, fewer cans of soda, and more vegetables. Also not an overnight win, but a gradual one over time. 

Anti-bias is exercise. We start off bad at it, over time we get better and better. Little by little, word by word, action by action we improve. Our biases are removed, our ability to fight our own biases (and those of others) gets stronger. 

When you lift weights, there's always someone stronger, someone who can lift better than you, or maybe is stronger than you in a different way. Similarly, it's a never ending process to try strengthen your anti-bias. There's always some way you can make yourself better.

Eating healthier is personal improvement. Exercise is personal improvement. Eliminating bias is personal improvement. 

Exercise your language and actions. Eliminate biases over time. Make yourself better.

Make your anti-bias swole.