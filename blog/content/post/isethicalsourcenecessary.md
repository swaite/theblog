---
title: "Isethicalsourcenecessary"
date: 2021-03-07T17:56:23-05:00
draft: true
---

tl;dr? Yes.

My opinion (for whatever that's worth) is a bit more nuanced than a one word answer.

### What is Ethical Source?

Ethical Source is a growing movement stemming from the Free and Open Source movements that seeks to prevent code (plus hardware, art, etc) released under an Ethical Source License from being used in a manner that the license author (and, by extension, person applying that license to the source) find unethical. This ranges from mass surveillance, abuse, environmental concerns and so on.

In 2019, the []Organization for Ethical Source](https://ethicalsource.dev/) was founded, with the mission to support and evangelize Ethical Source Licenses. Much like the [Free Software Foundation](https://www.fsf.org/) and [Open Source Initiative](http://opensource.org/) before them, they created a [definition](https://ethicalsource.dev/definition/) of what they believe Ethical Source should be, and have created a working group to drive their goals.

Reception has been mixed. Bruce Perens, one of the co-founders of the Open Source Initiative, has been [fairly critical](https://perens.com/2019/10/12/invasion-of-the-ethical-licenses/) of the licenses. A number of others have called to question similar problems. 

Is this necessary? Can Ethical Source Licenses actually pass legal muster?

I don't know, I don't pretend to be a lawyer. There's definitely significant concerns that they will not. I recently applied to be a supporting member of the OSE in order to help support them in their mission to create a license that will pass legal muster while also allowing devs to hold true to their beliefs. I likely won't ever use one in anything I've written, but I understand and support those that do. They're also working to create licenses that pass both the ESD as well as the OSD.

### My concerns

There's a few things that give me pause about the idea, that make me believe that the use of Ethical Source Licenses is fairly limited.

* We have significant problems enforcing usage of existing FOSS licenses. GPL'd code is found in various places, modified without attribution or sharing of that code. It takes a lot of money to fight this and honestly I'm not sure that we're winning a lot of those.
* The legal enforcement is difficult. Organizations that would abide the license, but disagree with them, simply won't use that code. Country-level organizations adopting a package licensed under an Ethical License will simply ignore the terms - this happens even with OSS. It will limit it's use in unethical ways, but not stop it unless there's truly a "killer app" created licensed this way. It's hard to imagine something so Earth shaking that it would change the course of humanity, but hey I won't rule it out.
* A large number of independent developers writing code using Open Source Licenses are unable to support themselves by selling that code. Critical packages are basically being written by single developers living in poverty just to keep the thing working. Corporate support of Ethical Licensed code is unlikely to be at all widespread, and almost all ethical code will be written by either small orgs or hobbyists. Not dissimilar from some OSS packages now, but without the support of the larger corporations that have adopted a lot of OSS applications and packages the use will be more limited, and money more limited. This is fine, just not an ideal situation. Given some of the pushback against large orgs adopting OSS, this may be considered a feature.
* If Ethical Source becomes in use in a widespread manner, that means that the ethics those licenses are pushing have also become a norm. Is the license required at all at that point? Would the GPL or similar accomplish the same thing at that point? 

### The good side

It's not all bad, and there's a reason why I said it is necessary.

* Developers shouldn't be concerned about their code being used in ways they don't want, if they don't want. These licenses can (somewhat) enforce that.
* The licenses raise awareness for the ethical concerns they address, and that's never a bad thing.